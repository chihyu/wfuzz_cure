import urllib2

url="https://olt-content.sans.org/6ef4b7a5-134b-40c3-85ab-0fd2f66e0e5c/video/002-720.webm"  
cookie="CloudFront-Policy=eyJTdGF0ZW1lbnQiOlt7IlJlc291cmNlIjoiaHR0cHM6Ly9vbHQtY29udGVudC5zYW5zLm9yZy82ZWY0YjdhNS0xMzRiLTQwYzMtODVhYi0wZmQyZjY2ZTBlNWMvKiIsIkNvbmRpdGlvbiI6eyJEYXRlTGVzc1RoYW4iOnsiQVdTOkVwb2NoVGltZSI6MTQ4NDkzMzc2NX19fV19; CloudFront-Signature=uBgy96vOlVqCXts~iBZyeVhUT9r9lZt3XbvSgL1dQdPeRg9S820zhW~DhrgxVozq2IjOzrDxbADgC2Vk~jiEAlpBgkZk2Jb8pJbcYcSLXMcR1DvIEh30Dpm~LzAbtqOFJr5~KXnZ~~DbkRLxlzQzrF4anyakCuW~hvnYFMFemXV1c9I-90Tw1yjZTqxWr3GBSCmrUavpfz75TMXpba~YS~lUpQ7laGieJa9JCSvDbspLZK2zdscgs1ojao7b6QPszBMI4VXsyrRiHroV1mk--HaE9KIUDdZgslUQweqazploQi4syHHRhS289cT55h9Y43ivRYh6OFVhERwXlsHwbw__; CloudFront-Key-Pair-Id=APKAJ7CMX2GMBBBHEH5A; QSI_HistorySession=https%3A%2F%2Fwww.sans.org%2F~1484760409004; __zlcmid=eegA0FPiqnUV04"
file_name=url.split("/")[-1]
opener=urllib2.build_opener()
opener.addheaders.append(('Cookie',cookie))
try:
	u=opener.open(url)
except IOError as e:
	print e

f=open(file_name,'wb')
meta = u.info()
file_size = int(meta.getheaders("Content-Length")[0])

file_size_dl = 0
block_sz = 8192
while True:
	buffer = u.read(block_sz)
	if not buffer:
		break

	file_size_dl += len(buffer)
	f.write(buffer)
	status = r"%10d [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
	status = status + chr(8)*(len(status)+1)
	print status,

