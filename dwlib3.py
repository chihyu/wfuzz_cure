import urllib3
#import PoolManager
#from urllib3 import PoolManager
#from urllib3.poolmanager import PoolManager
import requests

http = urllib3.PoolManager()
url = ("https://www.cca.edu/sites/default/files/pdf/08/word-to-pdf.pdf")
url = ("https://olt-content.sans.org/6ef4b7a5-134b-40c3-85ab-0fd2f66e0e5c/video/001-720.webm")
file_name = url.split('/')[-1]
myheaders = {'Cookie':'CloudFront-Policy=eyJTdGF0ZW1lbnQiOlt7IlJlc291cmNlIjoiaHR0cHM6Ly9vbHQtY29udGVudC5zYW5zLm9yZy82ZWY0YjdhNS0xMzRiLTQwYzMtODVhYi0wZmQyZjY2ZTBlNWMvKiIsIkNvbmRpdGlvbiI6eyJEYXRlTGVzc1RoYW4iOnsiQVdTOkVwb2NoVGltZSI6MTQ4NDg0NjgzNH19fV19; CloudFront-Signature=PI9Ud9mo0Hfkb03dRLsdilcxgQVnjb~zBe5FcPMbSxU3UNIMr-pvc--GQsUvdjEsgz~hNf-h6M3VX0c8RkI-3rMj9tCOVWQxXGn5xHdR5y4-NiX0RGuTwGx0I~kx0BFZhwAwOggvAr3~TJJngTeVRAbzUWzWVrzY5Q3j7Ke-NkvnkYCbkcEr2Yris6glsnxQnVawWXjjUpooi4ZCru2U7n1tNTeeY~GzRwqKqU~JFKLOmalMX61fhD7js05991LzNt-hSKLGMEpK-vVyR5fl-JmO4RhxiKk2S-r0chY0AzUH9PBq964Huxxkf8tRxrW1RxCIFU30vJF3yRtzghti~w__; CloudFront-Key-Pair-Id=APKAJ7CMX2GMBBBHEH5A; QSI_HistorySession=https%3A%2F%2Fwww.sans.org%2F~1484760409004; __zlcmid=eegA0FPiqnUV04'}
r = http.request('GET', url, preload_content=False,headers=myheaders,verify=True)
chunk_size=8192
with open(file_name, 'wb') as out:
    while True:
        data = r.read(chunk_size)
        if not data:
            break
        out.write(data)

r.release_conn()
